function createCard(title, subtitle, description, pictureUrl, formattedStart, formattedEnd) {
    return `
    <div class="col">
        <div class="card shadow mb-3">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
            <h5 class="card-title">${title}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
            <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">${formattedStart} - ${formattedEnd}</div>
        </div>
    </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
try {
    const response = await fetch(url);

    if (!response.ok) {
        console.log("Roast Beef!!! ${response.status}");
    } else {
      const data = await response.json();


      for(let conference of data.conferences){

      const detailUrl = `http://localhost:8000${conference.href}`;
      const detailResponse = await fetch(detailUrl);
      if (detailResponse.ok){
        const details = await detailResponse.json();
        const title = details.conference.name;
        const description = details.conference.description;
        const pictureUrl = details.conference.location.picture_url;
        const subtitle = details.conference.location.name
        const formattedStart = new Date(details.conference.starts).toLocaleDateString("en-US", {month:"numeric", day:"numeric", year:"numeric"});
        const formattedEnd = new Date(details.conference.ends).toLocaleDateString("en-US", {month:"numeric", day:"numeric", year:"numeric"});
        const html = createCard(title, subtitle, description, pictureUrl, formattedStart, formattedEnd);
        const column = document.querySelector('.row');
        column.innerHTML += html;
      }
    }
    
    }
  } catch (e) {
    console.error("error ${e}");

  }

});
